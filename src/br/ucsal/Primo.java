package br.ucsal;

        import java.util.Scanner;

public class Primo {

    public int leitor(){
        System.out.println("Digite um número: ");
        int s = new Scanner(System.in).nextInt();
        if (s < 1 || s > 10000){
            return 0;
        }
        return s ;
    }

    public boolean calculo(int num){
        for(int i = 2; i < (num / 2); i++){
            if (num % i == 0){
                return false;
            }
        }

        return true;
    }

    public boolean isPrimo(int valor){
        int n = valor;
        if (calculo(n)){
            System.out.print("O número " +n+ " é primo.\n");
            return true;
        }else{
            System.out.print("O número " +n+ " não é primo.\n");
        }

        return false;
    }
}
