package teste;

import br.ucsal.Primo;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.runners.Enclosed;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Collection;

@RunWith(Enclosed.class)
public class PrimoTeste {

    @RunWith(Parameterized.class)
    public static class PrimoParametrizadoTeste{
        private final Integer entrada;
        private final Boolean esperado;
        private Primo primo;

        public PrimoParametrizadoTeste(Integer entrada, Boolean esperado) {
            this.entrada = entrada;
            this.esperado = esperado;
        }

        @Before
        public void inicializar(){
            primo = new Primo();
        }

        @Parameterized.Parameters
        public static Collection testes() {
            return Arrays.asList(new Object[][] {{7, true}, {631, true}, {3, true}, {11, true},
                    {8, false}, {2, true}, {10, false}, {15, false}, {211, true}, {21, false}});
        }

        @Test
        public void testPrimo() {
            System.out.println("Entrada: "+ entrada);
            assertEquals(esperado, primo.calculo(entrada));
        }
    }

    public static class EntradaSaidaTeste{
        @Test
        public void testEntrada1(){
            ByteArrayInputStream in = new ByteArrayInputStream("26\n".getBytes());
            System.setIn(in);
            Primo primo = new Primo();
            assertEquals(false, primo.isPrimo(primo.leitor()));
        }

        @Test
        public void testEntrada2(){
            ByteArrayInputStream in = new ByteArrayInputStream("131\n".getBytes());
            System.setIn(in);
            Primo primo = new Primo();
            assertEquals(true, primo.isPrimo(primo.leitor()));
        }

        @Test
        public void testSaida1(){
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            System.setOut(new PrintStream(out));

            Primo primo = new Primo();
            primo.isPrimo(22);
            String resultadoAtual = out.toString();

            String resultadoEsperado = "O número 22 não é primo.\n";
            assertEquals( resultadoEsperado, resultadoAtual);
        }

        @Test
        public void testSaida2(){
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            System.setOut(new PrintStream(out));

            Primo primo = new Primo();
            primo.isPrimo(17);
            String resultadoAtual = out.toString();

            String resultadoEsperado = "O número 17 é primo.\n";
            assertEquals(resultadoEsperado, resultadoAtual);
        }
    }

}